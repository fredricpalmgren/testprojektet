//using ConsoleApp.Model;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace TestProjektet
{
    [TestFixture]
    public class TestTransform : TestBase
    {

        private static IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        private static string ProjectPath = Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        private static string SSISProjectName = "Transform";
        private static string SSISPath = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, SSISProjectName);

        public TestTransform()
        {
        }

        [OneTimeSetUp]
        public void Init()
        {
            // s�kv�gar till testdata samt setupscript
            string inputFile = Path.Combine(ProjectPath, "testdata\\testdata.sql");
            string setupFile = Path.Combine(ProjectPath, "scripts\\setupTables.sql");
            
            string createLandingFile = Path.Combine(ProjectPath, "scripts\\createLanding.sql");
            string createStagingFile = Path.Combine(ProjectPath, "scripts\\createStaging.sql");
            config["landingTestDBName"] = SetupEnvironment(config["ConnectionStrings:master"], createLandingFile, "landing");
            config["stagingTestDBName"] = SetupEnvironment(config["ConnectionStrings:master"], createStagingFile, "staging");

            config["ConnectionStrings:landingTest"] = config["ConnectionStrings:landing"].Replace("landing", config["landingTestDBName"]);
            config["ConnectionStrings:stagingTest"] = config["ConnectionStrings:staging"].Replace("staging", config["stagingTestDBName"]);

            // Se till att vi har r�tt tabeller att jobba med
            //RunScript(config["ConnectionStrings:landingTest"], setupFile);

            // Fyll p� med data till landing
            AddData(config["ConnectionStrings:landingTest"], inputFile);

            //RunDTSX(Path.Combine(SSISPath, "Package.dtsx"));

            // K�r SSISpaketet p� servern
            RunSSIS(config["ConnectionStrings:landingTest"],
                "Test",
                "Project",
                "Package.dtsx",
                config["ConnectionStrings:landingTest"],
                config["ConnectionStrings:stagingTest"]);

            //Testskott som funkar
            //RunProc(config["ConnectionStrings:landingTest"], "execute_ssis_package", null);
            //RunAgentJob(config["ConnectionStrings:landingTest"], "Run Package");
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            //string resetFile = Path.Combine(ProjectPath, "scripts\\resetTables.sql");
            //RunScript(config["ConnectionStrings:landingTest"], resetFile);

            //DropTestDB(config["ConnectionStrings:master"], config["landingTestDBName"]);
            //DropTestDB(config["ConnectionStrings:master"], config["stagingTestDBName"]);
        }

        public void TestIfDAX()
        {
            var results = RunDaxQuery(config["ConnectionStrings:conn"], $@"EVALUATE ( SUMMARIZECOLUMNS ('�rendetyp'[�rendetyp],""Antal"", [Antal �renden] ))");

            var serviceMatterType = results.Rows[0].Field<string>(0);
            var NumberOfServiceMatters = results.Rows[0].Field<int>(1);

            Assert.AreEqual(5, NumberOfServiceMatters);
        }

        [Test]
        [Order(1)]
        public void TestIfTestTableExist()
        {
            // Vi vill ha en tr�ff. S� blir k�rningen true s� �r det lyckat
            var result = RunBoolQuery(config["ConnectionStrings:stagingTest"], "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'targettable'");

            Assert.IsTrue(result);
        }

        [Test]
        [Order(2)]
        public void TestIfTestTableContainsNull()
        {
            // Vi vill inte ha n�gra tr�ffar. S� blir k�rningen false s� �r det lyckat
            var result = RunBoolQuery(config["ConnectionStrings:stagingTest"], "select * from (select * from targettable where null in ('varcharval', 'datetimeval')) AS Result");

            Assert.IsTrue(!result);
        }

        [Test]
        [Order(3)]
        public void TestIfColumnDataHasRightSize()
        {
            // Test gjort f�r att faila
            // Vi vill inte ha n�gra tr�ffar
            // Kravet �r att alla v�rden i kolumnen varcharval ska ha minst 2 tecken
            bool result = RunBoolQuery(config["ConnectionStrings:stagingTest"], "SELECT * FROM targettable WHERE LEN(varcharval) < 3");

            Assert.IsTrue(!result);
        }

        [Test]
        [Order(4)]
        public void TestIfTableHasCorrectRows()
        {
            // Kontrollera om targettable inneh�ller 5 rader
            int result = RunCountQuery(config["ConnectionStrings:stagingTest"], "SELECT count(*) FROM targettable");

            Assert.IsTrue(result == 5);
        }
    }
}