﻿using Microsoft.AnalysisServices.AdomdClient;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.SqlServer.Management.IntegrationServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace TestProjektet
{
    public class TestBase
    {

        /// <summary>
        /// Inserts testdata into the database
        /// </summary>
        /// <param name="connString">Connectionstring</param>
        /// <param name="dataFile">Path to datafile</param>
        protected void AddData(string connString, string dataFile)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {

                    connection.Open();
                    string testData = File.ReadAllText(dataFile);

                    string[] insertStatements = testData.Split(new string[] { ";\r\n", ";\n" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < insertStatements.Length; i++)
                    {
                        using (SqlCommand command = new SqlCommand(insertStatements[i], connection))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        protected bool DropTestDB(string connString, string dbName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    String dropSql = @"ALTER DATABASE " + dbName + @" SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
                                    DROP DATABASE [" + dbName + "]";

                    using (SqlCommand command = new SqlCommand(dropSql, connection))
                    {
                        command.ExecuteNonQuery();

                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return false;
        }


        /// <summary>
        /// Returnerar true om körd SQL ger ett resultat, annars false
        /// </summary>
        /// <param name="connString">Connectionstring</param>
        /// <param name="scriptFilePath">Sökväg till ett SQL skript</param>
        /// <param name="sqlString">SQL-sats</param>
        /// <returns>bool</returns>
        protected bool RunBoolQuery(string connString, string sqlString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sqlString, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return reader.HasRows;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return false;
        }


        /// <summary>
        /// Returnerar en count. Används när resultatet ska bli en int
        /// </summary>
        /// <param name="connString">Connectionstring</param>
        /// <param name="sqlString">SQL-sats</param>
        /// <returns>bool</returns>
        protected int RunCountQuery(string connString, string sqlString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sqlString, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                reader.Read();
                                return reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return -1;
        }

        protected DataTable RunDaxQuery(string connString, string query)
        {
            var tabularResults = new DataTable();
            using (AdomdConnection connection = new AdomdConnection(connString))
            {
                connection.Open();
                var currentDataAdapter = new AdomdDataAdapter(query, connection);
                currentDataAdapter.Fill(tabularResults);
            }

            return tabularResults;
        }


        protected List<string> RunScript(string connString, string scriptFilePath)
        {
            return RunQueryList(connString, scriptFilePath, null);
        }

        /// <summary>
        /// Returnerar en List med innehåll om körd SQL ger ett resultat, annars null
        /// </summary>
        /// <param name="connString">Connectionstring</param>
        /// <param name="scriptFilePath">Sökväg till ett SQL skript</param>
        /// <param name="sql">SQL-sats</param>
        /// <returns>List eller null</returns>
        protected List<string> RunQueryList(string connString, string scriptFilePath, string sqlString)
        {
            List<string> ReturnValue = null;
            try
            {

                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    if (sqlString != null)
                    {
                        using (SqlCommand command = new SqlCommand(sqlString, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    ReturnValue = new List<string>();
                                    while (reader.Read())
                                    {
                                        for (int i = 0; i < reader.FieldCount; i++)
                                        {
                                            ReturnValue.Add(reader.GetString(i));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        string script = File.ReadAllText(@scriptFilePath);
                        IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                        foreach (string commandString in commandStrings)
                        {
                            if (commandString.Trim() != "")
                            {
                                using (SqlCommand command = new SqlCommand(commandString, connection))
                                {
                                    var result = command.ExecuteScalar();
                                    if (result != null)
                                    {
                                        ReturnValue = new List<string>();
                                    }
                                }
                            }
                        }
                        connection.Close();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return ReturnValue;
        }

        
        
        /// <summary>
        /// Kör en procedur från aktuell databas
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="proc">procedur med ev parametrar t ex sp_rename 'testtable_orig', 'testtable'</param>
        protected void RunProc(string connString, string proc, Hashtable parameters)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(proc, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    
                    foreach(DictionaryEntry param in parameters)
                    {
                        command.Parameters.Add(new SqlParameter((string)param.Key, (string)param.Value));
                    }
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        /// <summary>
        /// Triggar ett SSIS-paket
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="folder"></param>
        /// <param name="project"></param>
        /// <param name="package"></param>
        /// <param name="landingConn"></param>
        /// <param name="stagingConn"></param>
        protected void RunSSIS(string connString, string folder, string project, string package, string landingConn, string stagingConn)
        {
            Hashtable parameters = new Hashtable
            {
                ["@folder"] = folder,
                ["@project"] = project,
                ["@package"] = package,
                ["@LandingConn"] = landingConn,
                ["@StagingConn"] = stagingConn
            };
            RunProc(connString, "RunSSISPackage", parameters);
        }

        /// <summary>
        /// Triggar ett agentjobb. Notera att det här körs asynkront, så vi kan inte vänta på resultatet
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="agentName"></param>
        protected void RunAgentJob(string connString, string agentName)
        {
            Hashtable parameters = new Hashtable();
            parameters["@job_name"] = agentName;
            RunProc(connString, "msdb.dbo.sp_start_job", parameters);
            //RunProc(connString, "msdb.dbo.sp_start_job @job_name = '" + agentName + "'", null);
        }

        /// <summary>
        /// Kör ett SSIS-paket från SSISDB
        /// </summary>
        /// <param name="connString">Connection string</param>
        /// <param name="folderName">Mappanamn i katalogen</param>
        /// <param name="projectName">Projekt i mappen</param>
        /// <param name="packageName">Namn på paket att köra</param>
        protected void RunSSISFromDB(string connString, string folderName, string projectName, string packageName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();
                    
                    IntegrationServices integrationServices = new IntegrationServices(connection);

                    Catalog catalog = integrationServices.Catalogs["SSISDB"];
                    CatalogFolder folder = catalog.Folders[folderName];
                    ProjectInfo project = folder.Projects[projectName];
                    Microsoft.SqlServer.Management.IntegrationServices.PackageInfo package = project.Packages[packageName];


                    package.Execute(false, null);
                    
                }

            }

            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        protected void RunDTSX(string dtsxPath)
        {

            Package pkg;
            Application app;
            DTSExecResult pkgResults;

            app = new Application();
            pkg = (Package)app.LoadPackage(dtsxPath, null);
            pkgResults = pkg.Execute();

            Console.WriteLine(pkgResults.ToString());

        }

        protected string SetupEnvironment(string connString, string scriptFilePath, string name)
        {
            string dbName = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    dbName = name + "_" + DateTime.Now.Ticks;
                    string script = File.ReadAllText(@scriptFilePath);
                    script = script.Replace(name, dbName);

                    IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                    foreach (string commandString in commandStrings)
                    {
                        if (commandString.Trim() != "")
                        {
                            using (SqlCommand command = new SqlCommand(commandString, connection))
                            {
                                var result = command.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return dbName;
        }
    }
}
