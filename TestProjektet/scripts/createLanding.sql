﻿USE [master]
GO
/****** Object:  Database [landing]    Script Date: 2021-11-19 11:44:09 ******/
CREATE DATABASE [landing]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'landing', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\landing.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'landing_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\landing_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [landing] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [landing].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [landing] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [landing] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [landing] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [landing] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [landing] SET ARITHABORT OFF 
GO
ALTER DATABASE [landing] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [landing] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [landing] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [landing] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [landing] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [landing] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [landing] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [landing] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [landing] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [landing] SET  DISABLE_BROKER 
GO
ALTER DATABASE [landing] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [landing] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [landing] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [landing] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [landing] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [landing] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [landing] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [landing] SET RECOVERY FULL 
GO
ALTER DATABASE [landing] SET  MULTI_USER 
GO
ALTER DATABASE [landing] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [landing] SET DB_CHAINING OFF 
GO
ALTER DATABASE [landing] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [landing] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [landing] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [landing] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'landing', N'ON'
GO
ALTER DATABASE [landing] SET QUERY_STORE = OFF
GO
USE [landing]
GO
/****** Object:  Table [dbo].[testtable]    Script Date: 2021-11-19 11:44:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[testtable](
	[varcharval] [varchar](100) NULL,
	[intval] [int] NULL,
	[datetimeval] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[RunSSISPackage]    Script Date: 2021-11-19 11:44:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Fredric Palmgren
-- Create date: 2021-11-18
-- Description:	Runs specified SSIS Package
-- =============================================
CREATE PROCEDURE [dbo].[RunSSISPackage]
    @folder nvarchar(50),  
	@project nvarchar(50),
	@package nvarchar(50),
	@LandingConn nvarchar(100),
	@StagingConn nvarchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @execution_id bigint


	 exec ssisdb.catalog.create_execution 
	  @folder_name = @folder
	 ,@project_name = @project
	 ,@package_name = @package
	 ,@execution_id = @execution_id output

	 exec ssisdb.catalog.set_execution_parameter_value
        @LandingConnectionString = @LandingConn  
       ,@StagingConnectionString = @StagingConn

	 exec ssisdb.catalog.start_execution @execution_id
END
GO
USE [master]
GO
ALTER DATABASE [landing] SET  READ_WRITE 
GO
