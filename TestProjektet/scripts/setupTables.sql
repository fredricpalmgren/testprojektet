﻿USE [landing]
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'testtable')
BEGIN
  EXEC sp_rename 'testtable', 'testtable_orig';
  CREATE TABLE testtable(
    varcharval varchar(100),
    intval int,
    datetimeval datetime);
END