﻿USE [landing]
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'testtable_orig')
BEGIN
  DROP TABLE testtable;
  EXEC sp_rename 'testtable_orig', 'testtable';
END